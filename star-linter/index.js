const ring = require('../star-ring/common')

module.exports.activate = ring.star_linter_activate
module.exports.deactivate = ring.star_linter_deactivate
module.exports.ui = ring.star_linter_ui
