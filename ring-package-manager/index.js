const ring = require('../star-ring/common')

module.exports.activate = ring.ring_package_manager_activate
module.exports.deactivate = ring.ring_package_manager_deactivate
module.exports.provider = ring.ring_package_manager_provider
