const ring = require('../star-ring/common')

module.exports.activate = ring.minivac_activate
module.exports.deactivate = ring.minivac_deactivate
