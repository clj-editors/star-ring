# Minivac

A chat-gpt (and possibly others) assistant for Pulsar

## What's with the name?

Inspired by the super-computer defined by Isaac Asimov (Multivac). The most
advanced version of Multivac is presented on "The Last Question" as Microvac.
As we're not yet on this state of advancement, I decided to name it "Minivac".
