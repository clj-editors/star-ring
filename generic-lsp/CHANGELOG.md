## Rolling Release
- Added support for command `generic-lsp:go-to-implementation`
- Added support for command `generic-lsp:find-references`
- Added support to search for multiple definitions/declarations
- Fixed prefix for some autocompletions
- Fixed datatip when a scope didn't exist under the cursor
- Fixed star-linter's UI opening

## Generic LSP 0.1.3 - Rust and Lua
- Adding support for rust-analyzer
- Adding support for lua-language-server
- Adding datatips if the package is present

## Generic LSP 0.1.2 - More languages support
- Adding preliminary snippet support
- Ruby added
- Added configuration that allows the user to define their own LSP servers to connect

## Generic LSP 0.1.0 - First Release
- Start a "known LSP" server (Clojure for now)
- Linter
- Go To Definition
- Go To Declaration
- Go To Type Declaration
- Autocomplete
