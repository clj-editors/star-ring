(ns generic-lsp.provider
  (:require [generic-lsp.commands :as cmds]
            [promesa.core :as p]))

(defn- ^:inline active-editor! [] (.. js/atom -workspace getActiveTextEditor))

(defn- from-editor
  ([] (.. (active-editor!) getGrammar -name))
  ([^js editor] (.. editor getGrammar -name)))

(defn- position-from-editor
  ([] (clj->js (cmds/position-from-editor (active-editor!))))
  ([editor] (clj->js (cmds/position-from-editor editor))))

(defn- location-from-editor
  ([] (clj->js (cmds/location-from-editor (active-editor!))))
  ([editor] (clj->js (cmds/location-from-editor editor))))

(defn send-command! [language command params]
  (p/then (cmds/send-command! language command params)
          clj->js))

(defn provider []
  #js {:runCommand send-command!
       :getServerFromEditor from-editor
       :uriFromFile cmds/file->uri
       :locationFromEditor location-from-editor
       :positionFromEditor position-from-editor})
