const ring = require('../star-ring')

module.exports.activate = ring.vs-activate
module.exports.deactivate = ring.vs-deactivate
